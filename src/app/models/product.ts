export class Product {
    pid: number;
    name: string;
    price: number;
    image: string;
    description: string;
}

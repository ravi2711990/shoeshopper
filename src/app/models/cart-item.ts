import { Product } from './product';

export class CartItem {
    cartId: number;
    product: Product;
    qty: number;

    constructor(cartId: number, product: Product,  qty = 1){
        this.cartId = cartId;
        this.product = product;
        this.qty = qty;
    }
}

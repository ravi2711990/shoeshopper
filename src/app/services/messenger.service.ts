import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Product } from '../models/product';
import { Register } from '../models/register';

@Injectable({
  providedIn: 'root'
})
export class MessengerService {

  subject = new Subject();
  constructor() { }

  sendMsg(product: Product){
    console.log(product);
    this.subject.next(product); //Triggering an event
  }

  getMsg(){
    return this.subject.asObservable()
  }

  sendUser(user: Register){
    console.log(user);
    this.subject.next(user);
  }

  getUser(){
    return this.subject.asObservable()
  }
}

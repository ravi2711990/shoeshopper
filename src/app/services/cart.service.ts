import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CartItem } from '../models/cart-item';
import { HttpClient } from '@angular/common/http'
import { Product } from '../models/product';
import { cartUrl } from 'src/app/config/api';
import { map } from 'rxjs/operators';
import { ProductService } from './product.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  cartItem: CartItem;
  cartId: number = 1;
  result: number;

  constructor(
    private http: HttpClient) { }

  getCartItems(): Observable<CartItem[]>{
    return this.http.get<CartItem[]>(cartUrl+"s").pipe(
      map((result: any[]) => {
        let cartItems: CartItem[] = [];
        
        for(let item of result){
          let productExists = false
          
          for(let i in cartItems){
            if(cartItems[i].product.pid === item.product.pid){
                cartItems[i].qty++
                productExists = true
                break;
            }
          }
      
          if(!productExists){
            cartItems.push(new CartItem(item.cartId, item.product))
        }
        }  
        return cartItems;
      })
    );
  }

  addProductToCart(product: Product): Observable<any>{
    this.result = this.cartId;
    this.cartItem = new CartItem(this.cartId,product);
    this.cartId++;
    return this.http.post(cartUrl, this.cartItem);
  }
}

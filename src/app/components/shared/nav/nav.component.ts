import { Component, OnInit } from '@angular/core';
import { MessengerService } from 'src/app/services/messenger.service';
import { Register } from 'src/app/models/register';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  users: Register;
  name: string;

  constructor(private msgservice: MessengerService) { }

  ngOnInit() {
    this.msgservice.getUser().subscribe((data: Register)=>{
      this.users = data;
      this.name = this.users.name
  })
  }
}
